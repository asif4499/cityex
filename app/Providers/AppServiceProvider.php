<?php

namespace App\Providers;

use App\Models\Delivery;
use App\Models\PaymentRequests;
use App\Models\PublicMessage;
use App\User;
use Facade\FlareClient\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
		$this->app->bind('path.public', function() {
			return realpath(base_path().'/../public_html');
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $aReqs = User::where('role_id', '2')->where('status', '2')->orderBy('id', 'desc')->with('district')->get();
        $aReq = count($aReqs);

        $mReqs = User::where('role_id', '3')->where('status', '2')->orderBy('id', 'desc')->with('district')->get();
        $req = count($mReqs);

        $dReqs = Delivery::where('delivery_status', 1)->get();
        $dReqC = count($dReqs);

        $prReqs = PaymentRequests::where('status', 2)->get();
        $prReqC = count($prReqs);

        $m = PublicMessage::where('seen', 2)->get();
        $mCount = count($m);

        view()->share(['mReq' => $req, 'dReq' => $dReqC, 'prReq' => $prReqC, 'm' => $mCount, 'aReq' => $aReq]);
    }
}
